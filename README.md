<h1 align="center">Padrões de Projeto</h1>


<p align="center"> Os padrões de projetos desenvolvidos aqui nessa biblioteca serão utilizados como ferramenta para soluções para problemas recorrentes no desenvolvimento de software, potencializando diretamente no aumento da qualidade do código tornando-o mais flexível, elegante e reusável, facilitando a comunicação entre as áreas de desenvolvimento e promovendo uma língua única entre a equipe. </p><br>

> <h4 align="center"> 🚧 Padrão de Projetos 🚀 está em construção... Aguarde! 🚧</h4>
<br>

 <h2> 🏁 Sumário</h2>
:one:  <a href="#Introduction">Introdução</a><br>
:two:  <a href="#apex"> Apex</a> <br>
:three: <a href="#arqstatic">Arquivos Estáticos</a> <br>
:four: <a href="#code">Código PL/SQL</a>  
</p>
