<h1  align="center">Classic Report With Form Modal Dialog</h1>

<p  align="center">Este conteúdo tem como objetivo auxiliar os futuros programadores front-end (e quem sabe os back-ends curiosos) a entender o fluxo de criação de páginas, facilitando assim a fluidez e comunicação do nosso trabalho geral. </p>

O primeiro passo é entender a tela que será construída:

![Classic Report With Form Modal Dialog](https://blogs.ontoorsolutions.com/images/Open-Inline-Model-Dialog/dialog.gif)

Essa tela é composta por uma página inteira, com uma região e um botão (normalmente o botão terá legenda de Novo). Quando clicar no botão, irá abrir um form modal que irá permitir, na maioria dos casos, que o usuário do sistema cadastre alguma nova informação no banco de dados.

Como estamos trabalhando em um sistema real, nós já possuímos acesso ao banco de dados, isso significa que quando criarmos essa página, automaticamente nossa ferramenta Apex irá trazer as colunas e linhas já existentes do banco de dados **de acordo** com a tabela do banco que instruímos na hora de criar a página. 

Então, o próximo passo para iniciarmos a desenvolver realmente é: 

 - Quais colunas serão visíveis e quais serão ocultas?
 - Qual será o nome dos campos para o usuário identificar os inputs?

Dito isso, é importante salientar que aqui estamos criando uma página que teremos que editar em dobro, a página inteira e o modal, portanto as duas perguntas anteriores valem para ambos desenvolvimento. 

No passo-a-passo estarei identificando cada ação como Report e como Modal, sendo Report a página um, e Modal  a página aberta ao clicar no botão.

**Checklist:**

 - [ ] Report: APK sempre será oculta (é sempre a primeira coluna no Page Designer)
 Exemplo: BEM_WMS_EMP_ERP_ID.
 --
 
 - [ ] Report: Created, CreatedBy, Update e UpdateBy serão sempre ocultos, a não ser que dito explicitamente o contrário. 
 --
 
 - [ ] Modal: Em Created e Update, em Default colocar:
Type -> Function Body
Language -> PL/SQL
PL/SQL -> Function Body
**return sysdate;** 

Vai retornar a data automaticamente do usuário que crio ou atualizou.

**- Atenção: :round_pushpin: Nunca devemos esquecer de, dentro do function body, validar no botão para ficar verdinho.**

--
 - [ ] Modal: Iremos criar o processes Get PK.

O Get PK está dizendo que (devido sua bolinha vermelha no item) precisa de uma condição para aquela tarefa ser executada. Nesse caso especifico, quando o usuário clicar no botão Incluir (essa é a condição), irá criar/cadastrar uma nova coluna.

--Name: Get PK
--Type: Execute Code

--Source: Location Local Database
Language PL/SQL
--PL/SQL Code:               
**- :round_pushpin: Atenção: Nunca esquecer de validar no editor até ficar verdinho.**

:nomedacoluna := seq_nomedacoluna.nextVal();            
[Esse nome da coluna é o page item cujo é PK). 

E basicamente este código diz: me dá o próximo ID. Sempre adicionando +1.

**:radioactive: JAMAIS ESQUECER SERVER-SIDE CONDITION**

Server-Side Condition deverá ser: When Button Pressed (NOVO).
[Essa é a condição, pra funcionar o processes].

--

 - [ ] Modal: Processes Changelog. Serve para quando salvar.

--Name: Changelog
--PL/SQL code:

**:updateby := ai_utl_mn_usuario_id;
:update := sysdate;**

--Server-Side Condition: When Button Pressed: Salvar.

**:radioactive: JAMAIS ESQUECER SERVER-SIDE CONDITION**

--
