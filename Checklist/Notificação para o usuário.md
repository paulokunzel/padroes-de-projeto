O objetivo é mostrar para o usuário mensagem de sucesso conforme sua ação (neste caso, vinculada ao botão, mas poderia ser uma região).


![notificação_exemplo](https://i.ibb.co/TMHCT05/Notificacao.png)


Esse tutorial está sendo escrito a nível de aplicação, isso significa que toda vez que você precisar realizar esse processo em aplicações diferentes que não tenham Application Items e Static Application Files, essas sessões deverão ser cadastradas conforme os passos abaixo:

- Para instalar na aplicação, inicialmente iremos criar um Application Items. Isso será feito para tornar a variável (que nesse momento está vazia) global. 


![ai_mensagem_processamento](https://i.ibb.co/ZTkTkP4/App-Items-AI-MENSAGEM-PROCESSAMENTO.png)

--
Agora, em um arquivo externo, criamos uma função JavaScript que irá exibir uma mensagem de sucesso com a string retornada pelo AJAX.

A função é: 


    const  exibirMensagemSucesso = () => {
    
    const  nome = 'Exibir Mensagem'
    
    const  ajaxCallback = {
    dataType :  'text',
    success:  apex.message.showPageSuccess,
    error :  console.error
    }
    
    apex.server.process(nome, null, ajaxCallback);
    
    }

Inserimos a função em Static Application Files e a referenciamos na aplicação desejada dentro do JavaScript do User Interface. 

Isso significa que o Ajax agora está global, exibindo a mensagem.

![Ref](https://i.ibb.co/vvkq1sK/Ref-UAI.png)

Feito isso, estamos prontos para oficialmente mexer nas páginas. 
Caso sua aplicação já tenha esses passos anteriores, você pode seguir daqui:

Usaremos a variável global AI_MENSAGEM_PROCESSAMENTO (a variável que citei lá em cima que estava vazia), iremos atribuir a mensagem que desejamos (string) no **Form dentro do Processing**. 
Exemplo:

No processing:

<a href="https://imgbb.com/"><img src="https://i.ibb.co/9q4Gdjy/processing.png" alt="processing" border="0"></a>

Atribuindo o valor da string e condicionando ao botão:

<a href="https://imgbb.com/"><img src="https://i.ibb.co/5RQ69vB/atribuirstring.png" alt="atribuirstring" border="0"></a>
<a href="https://imgbb.com/"><img src="https://i.ibb.co/rwTY0VD/Exemplo-server-condition.png" alt="Exemplo-server-condition" border="0"></a>

Agora, iremos chamar com uma ação dinâmica o código externo AJAX(O javascript lá do começo) e também a string, dentro da visualização do usuário (ou seja, fora do Form):

<a href="https://ibb.co/8YVY7k5"><img src="https://i.ibb.co/HhQhHZz/executejavascript.png" alt="executejavascript" border="0"></a>



E pronto, executou normal!
