<h1 align="center">Setup :wrench: :hammer: </h1>

<h3 align="center"> Items da aplicação / Application Items (AI):</h3>

:star: Application Items são o equivalente a variáveis globais do sistema. A versão local deles é o o Page item.

-   Iniciar com AI_
-   Usar letras maiúsculas e separa palavras com '_'
    -   Exemplo: AI_EMPRESA_ID
-   Escopo padrão = Application. Usar global apenas para sistemas multi aplicação.
-   Criar AIs para cada o texto de cada Botão.
-   Setar valores utilizando um Application Process

<h3 align="center">Processos da aplicação / Aplication Processes:</h3>
--

:star:  Processos da aplicação são blocos de lógica que podem ser executados em certos momentos.
   
-   Nomes Descritivos por extenso (ex.: Carregar dados da sessão).
-   Apenas chamar uma Função ou Procedure.

<h3 align="center">Authentication/Authorization:</h3>
--

Verificar com Paulo para escrever. 
