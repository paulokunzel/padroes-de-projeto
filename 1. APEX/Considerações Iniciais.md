<h1 align="center"> :earth_americas: Regras Globais de Trabalho: </h1>

<h3 align="center"> Padrões adotados para desenvolvimento: </h3>

 - Todos componentes, funções, ações, listas e etc em **Português** :tongue:.

- Utilizar apenas chamadas de Funções, Métodos e Expressões.

- Evitar códigos na tela das aplicações, APEX consegue fazer quase tudo **sem linhas de código**. 
 

<h3 align="center"> Theme Roller: </h3>

- Não usar CSS se o Theme Roller pode estilizar. 

- **Caso seja necessário**, todo código CSS deverá ser centralizado dentro do Theme Roller, em Custom CSS:

![enter image description here](http://www.laureston.ca/wp-content/uploads/2019/07/Screen-Shot-2019-07-12-at-9.28.48-AM.png)
 
- O nome do arquivo do tema deve ser igual ao do Projeto.

- Se o mesmo tema seja utilizado dentro de múltiplas aplicações dentro do mesmo Workspace, utilizar a funcionalidade *"Subscribe"*.

 ![enter image description here](https://gitlab.com/paulokunzel/padroes-de-projeto/uploads/d97bff4cd1ebeb7351dd1a1a35357249/subsc.PNG)
